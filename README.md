This module clears the search filter whenever the filer path is changed. 
I mainly use the search filter to quickly find a subfolder to click on, and I no longer need the filter as soon as I've entered said subfolder.
