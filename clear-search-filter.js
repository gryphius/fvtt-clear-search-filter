Hooks.on('renderFilePicker', (app, html, data) => {    
    let firstSearchFilter =  app._searchFilters[0]; 
    let tfinput = firstSearchFilter._input;
    if(firstSearchFilter.query){
        tfinput.value="";
        firstSearchFilter.query = "";
        const event = new KeyboardEvent("keyup", {"key": "Enter", "code": "Enter"});
        firstSearchFilter.filter(event, firstSearchFilter.query);
    }
 });
